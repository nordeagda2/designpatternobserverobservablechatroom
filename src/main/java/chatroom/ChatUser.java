package main.java.chatroom;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by RENT on 2017-08-10.
 */
public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages = new ArrayList<>();
    private boolean isAdmin;

    public ChatUser(String nick, Integer id) {
        this.id = id;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            if (((Message) arg).getUserID() == this.getId()) {
                messages.add(((Message) arg).getMessage());
                System.out.println("Message to " + this.getNick() + " : "
                        + this.messages.get(messages.size() - 1));
            }
        } else {
            System.out.println("Invalid message type.");
        }
    }
}
