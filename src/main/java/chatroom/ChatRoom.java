package main.java.chatroom;

import com.sun.istack.internal.NotNull;

import java.util.*;

/**
 * Created by RENT on 2017-08-10.
 */
public class ChatRoom extends Observable {
    private static final List<String> adminNames = new ArrayList<>();
    private static Integer userCounter = 0;

    private Map<Integer, ChatUser> userMap = new HashMap<>();
    private String roomName;

    public ChatRoom(String givenRoomName) {
        this.roomName = givenRoomName;

        // add two nicks that are able to be administrators
        this.adminNames.add("Admin");
        this.adminNames.add("Administrator");
    }

    public void userLogin(@NotNull String nick) {
        for (ChatUser user : userMap.values()) {
            if (user.getNick().equals(nick)) {
                System.out.println(nick + " can not be logged in. User with this kick already exists.");
                return;
            }
        }

        // create user
        ChatUser chatUser = new ChatUser(nick, ++userCounter);

        // put this user into userMap
        userMap.put(userCounter, chatUser);

        // set as admin if nick is on the admin list
        if (adminNames.contains(nick)) {
            chatUser.setAdmin(true);
        }

        // add observer (observer-observable design pattern)
        addObserver(chatUser);
    }

    public void kickUser(int idKicked, int idAdmin) {
        if (!userMap.containsKey(idAdmin) ||
                !userMap.containsKey(idKicked)) {
            System.out.println("One of those id's does not exist.");
            return;
        }

        // both users exist and now check admin permissions of the second one
        if (userMap.get(idAdmin).isAdmin()) {
            userMap.remove(idKicked);
            System.out.println("User with id " + idKicked + " has been removed");
        } else {
            System.out.println("You have no privileges to kick users");
        }
    }

    public void sendMessage(int user, String message) {
        // create message
        Message givenMessage = new Message(user, message);

        // notify about new message
        setChanged();
        notifyObservers(givenMessage);
    }
}
