package main.java.chatroom;

/**
 * Created by RENT on 2017-08-12.
 */
public class Message {
    private int userID;
    private String message;

    public Message(int userID, String message) {
        this.userID = userID;
        this.message = message;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
