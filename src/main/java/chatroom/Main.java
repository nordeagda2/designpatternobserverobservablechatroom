package main.java.chatroom;

import java.util.Scanner;

/**
 * Created by RENT on 2017-08-12.
 */
public class Main {
    public static void main(String[] args) {
        ChatRoom room = new ChatRoom();
        Scanner scanner = new Scanner(System.in);
//        room.userLogin("Admin");
//        room.userLogin("Bartek");
//        room.sendMessage(2,"Siema");

        while (true){
            System.out.println("Please give an action");

            String[] action =scanner.nextLine().split(" ");
            switch (action[0].trim().toLowerCase()){
                case "login":
                    room.userLogin(action[1]);
                    break;
                case "send":
                    room.sendMessage(Integer.parseInt(action[1]),action[2]);
                    break;
                case "kick":
                    room.kickUser(Integer.parseInt(action[1]),Integer.parseInt(action[2]));
                    break;
            }
            if(action[0].equals("quit")){
                break;
            }
        }
    }
}
